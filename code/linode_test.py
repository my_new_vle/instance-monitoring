import requests
from bs4 import BeautifulSoup
from prometheus_client import start_http_server, Gauge
import time

LINODE_STATUS_PAGE_URL = 'https://status.linode.com/'
GITLAB_STATUS_PAGE_URL = 'https://status.gitlab.com/'
AKAMAI_STATUS_PAGE_URL = 'https://status.akamai.com/'

services_status = Gauge('service_status','Service_status(1=Operational, 0=Degraded)',['service','status'])


def fetch_linode_status():
    try:
        response = requests.get(LINODE_STATUS_PAGE_URL)
        soup =  BeautifulSoup(response.text,'html5lib')
        name_elements = soup.find_all('span', class_= 'component-status')
        status_elements = soup.find_all('span', class_= 'name')
        status = [element.text.strip() for element in name_elements]
        name = [element.text.strip() for element in status_elements]
    
        
        for name, status in zip(name, status):
            if status == 'Operational':
               services_status.labels(service=name,status='Operational').set(1)
            else:
               services_status.labels(service=name,status='Degraded').set(0)
            
        
            #print(f"Service_name: {name}, Service_Status: {status}")
#         print(services)
#         for service in services:
#             print(service.text.strip())
#             service_name = service.find('div',class_='component-inner-container status-green').text.strip()
#             status_text = service.find('span',class_='component-status').text.strip()
#             print(f"Service: {service_name}, Status:{status_text}")
    except Exception as e:
      print(f"Error fetching Linode Status:{e}")
      
def fetch_gitlab_status():
    try:
        response = requests.get(GITLAB_STATUS_PAGE_URL)
        soup =  BeautifulSoup(response.text,'html5lib')
        name_elements = soup.find_all('p', class_='pull-right component-status')
        status_elements = soup.find_all('p', class_='component_name')
        status = [element.text.strip() for element in name_elements]
        name = [element.text.strip() for element in status_elements]
    
        
        for name, status in zip(name, status):
            if status == 'Operational':
               services_status.labels(service=name,status='Operational').set(1)
            else:
               services_status.labels(service=name,status='Degraded').set(0)
            
        
            #print(f"Service_name: {name}, Service_Status: {status}")
#         print(services)
#         for service in services:
#             print(service.text.strip())
#             service_name = service.find('div',class_='component-inner-container status-green').text.strip()
#             status_text = service.find('span',class_='component-status').text.strip()
#             print(f"Service: {service_name}, Status:{status_text}")
    except Exception as e:
      print(f"Error fetching gitlab Status:{e}")

def fetch_akamai_status():
    try:
        response = requests.get(AKAMAI_STATUS_PAGE_URL)
        soup =  BeautifulSoup(response.text,'html5lib')
        name_elements = soup.find_all('span', class_='component-status')
        status_elements = soup.find_all('span', class_='name')
        status = [element.text.strip() for element in name_elements]
        print(status)
        name = [element.text.strip() for element in status_elements]
        print(name)
    
        
        for name, status in zip(name, status):
            if status == 'Operational':
               services_status.labels(service=name,status='Operational').set(1)
            else:
               services_status.labels(service=name,status='Degraded').set(0)
            
        
            #print(f"Service_name: {name}, Service_Status: {status}")
#         print(services)
#         for service in services:
#             print(service.text.strip())
#             service_name = service.find('div',class_='component-inner-container status-green').text.strip()
#             status_text = service.find('span',class_='component-status').text.strip()
#             print(f"Service: {service_name}, Status:{status_text}")
    except Exception as e:
      print(f"Error fetching akamai Status:{e}")
        

if __name__ == '__main__':
    start_http_server(8080)
    
    while True:
        fetch_linode_status()
        fetch_gitlab_status()
        fetch_akamai_status()
        time.sleep(60)